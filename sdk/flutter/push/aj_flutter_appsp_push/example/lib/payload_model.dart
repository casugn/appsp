class PayloadModel {
  ///声音资源
  String sound;
  ///其他字段，TODO

  PayloadModel(
      {this.sound});

  PayloadModel.fromJson(Map<String, dynamic> json) {
    sound = json['sound'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sound'] = this.sound;
    return data;
  }

  @override
  String toString() {
    return 'PayloadModel{sound: $sound}';
  }
}
