package com.anji.plus.ajpushlibrary.base;

import android.content.Context;

public class AppSpPushBaseController {
    protected Context context;
    protected String appKey;

    public AppSpPushBaseController(Context mContext, String appKey) {
        this.context = mContext;
        this.appKey = appKey;
    }

    public Context getContext() {
        return context;
    }

    public String getAppKey() {
        return appKey;
    }
}
