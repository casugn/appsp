package com.anji.sp.push.service.impl;

import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.common.DeviceType;
import com.anji.sp.push.model.po.PushConfiguresPO;
import com.anji.sp.push.model.po.PushUserPO;
import com.anji.sp.push.model.vo.RequestSendBean;
import com.anji.sp.push.service.PushService;
import com.anji.sp.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@Service
public class PushUtilServiceImpl {
    private static int patchNum = 1000;//批量发送最多数限制 华为是1000 极光1000
    @Autowired
    private HuaweiPushServiceImpl huaweiPushService;
    @Autowired
    private JPushServiceImpl jPushService;
    @Autowired
    private XiaomiPushServiceImpl xiaomiPushService;
    @Autowired
    private OppoPushServiceImpl oppoPushService;
    @Autowired
    private VivoPushServiceImpl vivoPushService;
    @Value("${custom.push.vivo.mode:1}")
    private int customPushMode;
    public ResponseModel pushBatchMsg(RequestSendBean requestSendBean, PushUserPO pushUserPO, PushConfiguresPO pushConfiguresPO, String msgId) throws Exception {
        if (pushConfiguresPO == null) {
            return ResponseModel.errorMsg("请输入配置信息");
        }
        if (Objects.isNull(requestSendBean.getAndroidConfig())) {
            requestSendBean.setAndroidConfig(new HashMap<>());
        }
        if (Objects.isNull(requestSendBean.getIosConfig())) {
            Map<String, Object> iosConfig = new HashMap<>();
            iosConfig.put("sound", "default");
            requestSendBean.setIosConfig(new HashMap<>());
        }
        //根据deviceId查询token registionid
        requestSendBean.setDeviceType(pushUserPO.getDeviceType());
        if (pushUserPO.getDeviceType().equals(DeviceType.IOS)) {
            ArrayList<String> registrationIds = new ArrayList<>();
            registrationIds.add(pushUserPO.getRegistrationId());
            requestSendBean.setRegistrationIds(registrationIds);
            jPushService.pushIos(requestSendBean, pushConfiguresPO, msgId, "5");
        } else {
            ArrayList<String> registrationIds = new ArrayList<>();
            ArrayList<String> tokens = new ArrayList<>();
            if (!StringUtils.isEmpty(pushUserPO.getRegistrationId())) {
                registrationIds.add(pushUserPO.getRegistrationId());
            }
            if (!StringUtils.isEmpty(pushUserPO.getManuToken())) {
                tokens.add(pushUserPO.getManuToken());
            }
            requestSendBean.setRegistrationIds(registrationIds);
            requestSendBean.setTokens(tokens);
            pushAndroidBatchMsg(requestSendBean, pushConfiguresPO, msgId);
        }

        return ResponseModel.success();
    }

    public ResponseModel pushAllMsg(RequestSendBean requestSendBean, ArrayList<PushUserPO> userBeansHW
            , ArrayList<PushUserPO> userBeansXM, ArrayList<PushUserPO> userBeansOP
            , ArrayList<PushUserPO> userBeansVO, ArrayList<PushUserPO> userBeansOther
            , ArrayList<PushUserPO> userBeansIOS, PushConfiguresPO pushConfiguresPO, String msgId) throws Exception {
        if (pushConfiguresPO == null) {
            return ResponseModel.errorMsg("请输入配置信息");
        }
        if (Objects.isNull(requestSendBean.getAndroidConfig())) {
            requestSendBean.setAndroidConfig(new HashMap<>());
        }
        if (Objects.isNull(requestSendBean.getIosConfig())) {
            Map<String, Object> iosConfig = new HashMap<>();
            iosConfig.put("sound", "default");
            requestSendBean.setIosConfig(new HashMap<>());
        }
        pushAndroidAllMsg(requestSendBean, userBeansHW, userBeansXM, userBeansOP, userBeansVO, userBeansOther, pushConfiguresPO, msgId);
        pushIosAllMsg(requestSendBean, userBeansIOS, pushConfiguresPO, msgId);
        return ResponseModel.success();
    }

    private void pushAndroidAllMsg(RequestSendBean requestSendBean, ArrayList<PushUserPO> userBeansHW
            , ArrayList<PushUserPO> userBeansXM, ArrayList<PushUserPO> userBeansOP
            , ArrayList<PushUserPO> userBeansVO, ArrayList<PushUserPO> userBeansOther
            , PushConfiguresPO pushConfiguresPO, String msgId) throws Exception {
        //添加声音
        if (!StringUtils.isEmpty(requestSendBean.getAndroidConfig())) {
            Map<String, String> extras = requestSendBean.getExtras();
            if (extras == null) {
                extras = new HashMap<>();
            }
            Object sound = requestSendBean.getAndroidConfig().get("sound");
            if (Objects.nonNull(sound)) {
                extras.put("sound", sound.toString());
            }
            requestSendBean.setExtras(extras);
        }
        if (StringUtils.isEmpty(requestSendBean.getPushType()) || "0".equals(requestSendBean.getPushType())) {
            ArrayList<String> tokensHW = new ArrayList<>();
            ArrayList<String> jgsHW = new ArrayList<>();
            ArrayList<String> tokensXM = new ArrayList<>();
            ArrayList<String> jgsXM = new ArrayList<>();
            ArrayList<String> tokensOP = new ArrayList<>();
            ArrayList<String> jgsOP = new ArrayList<>();
            ArrayList<String> tokensVO = new ArrayList<>();
            ArrayList<String> jgsVO = new ArrayList<>();
            ArrayList<String> tokensOther = new ArrayList<>();
            for (int i = 0; i < userBeansHW.size(); i++) {
                tokensHW.add(userBeansHW.get(i).getManuToken());
                jgsHW.add(userBeansHW.get(i).getRegistrationId());
            }
            for (int i = 0; i < userBeansXM.size(); i++) {
                tokensXM.add(userBeansXM.get(i).getManuToken());
                jgsXM.add(userBeansXM.get(i).getRegistrationId());
            }
            for (int i = 0; i < userBeansOP.size(); i++) {
                tokensOP.add(userBeansOP.get(i).getManuToken());
                jgsOP.add(userBeansOP.get(i).getRegistrationId());

            }
            for (int i = 0; i < userBeansVO.size(); i++) {
                tokensVO.add(userBeansVO.get(i).getManuToken());
                jgsVO.add(userBeansVO.get(i).getRegistrationId());

            }
            // 如果设备类型是别的也走极光推送 设定devicetype=0为其他手机走极光
            for (int i = 0; i < userBeansOther.size(); i++) {
                tokensOther.add(userBeansOther.get(i).getRegistrationId());
            }

            //批量发huawei
            pushAllMsgByPatch(tokensHW, jgsHW, requestSendBean, pushConfiguresPO, huaweiPushService, msgId, "1");
            //批量发xiaomi
            pushAllMsgByPatch(tokensXM, jgsXM, requestSendBean, pushConfiguresPO, xiaomiPushService, msgId, "2");
            //批量发op
            pushAllMsgByPatch(tokensOP, jgsOP, requestSendBean, pushConfiguresPO, oppoPushService, msgId, "3");
            //批量发vo
            if (customPushMode == 0){
                pushAllMsgByPatch(tokensVO, jgsVO, requestSendBean, pushConfiguresPO, vivoPushService, msgId, "4");
            }


            //极光批量发android
            pushAllMsgByPatch(null, tokensOther, requestSendBean, pushConfiguresPO, null, msgId, "6");
            //vivo目前都单推 全推用不了
            if (customPushMode == 1){
                for (int i = 0; i < userBeansVO.size(); i++) {
                    ArrayList<String> tokensSignVO = new ArrayList<>();
                    ArrayList<String> jgsSignVO = new ArrayList<>();
                    tokensSignVO.add(userBeansVO.get(i).getManuToken());
                    jgsSignVO.add(userBeansVO.get(i).getRegistrationId());
                    requestSendBean.setTokens(tokensSignVO);
                    requestSendBean.setRegistrationIds(jgsSignVO);
                    vivoPushService.pushBatchMessage(requestSendBean, pushConfiguresPO, msgId, "4");
                }
            }


        } else {
            //TODO 走极光
            ArrayList<String> registIds = new ArrayList<>();
            for (int i = 0; i < userBeansHW.size(); i++) {
                registIds.add(userBeansHW.get(i).getRegistrationId());
            }
            for (int i = 0; i < userBeansXM.size(); i++) {
                registIds.add(userBeansXM.get(i).getRegistrationId());
            }
            for (int i = 0; i < userBeansOP.size(); i++) {
                registIds.add(userBeansOP.get(i).getRegistrationId());
            }
            for (int i = 0; i < userBeansVO.size(); i++) {
                registIds.add(userBeansVO.get(i).getRegistrationId());
            }
            for (int i = 0; i < userBeansOther.size(); i++) {
                registIds.add(userBeansOther.get(i).getRegistrationId());
            }
            //极光批量发android
            pushAllMsgByPatch(null, registIds, requestSendBean, pushConfiguresPO, null, msgId, "6");

        }
    }

    /**
     * 批量推送
     *
     * @param tokens
     * @param requestSendBean
     * @param pushConfiguresPO
     * @param msgId
     * @param targetType
     */
    private void pushAllMsgByPatch(ArrayList<String> tokens, ArrayList<String> registrationIds, RequestSendBean requestSendBean, PushConfiguresPO pushConfiguresPO,
                                   PushService pushService, String msgId, String targetType) throws Exception {
        if (registrationIds.size() != 0) {
            //分批加载 1000个为一批
            int patch = registrationIds.size() / patchNum;
            for (int j = 0; j < patch + 1; j++) {
                List<String> patchTokens = new ArrayList<>();
                List<String> patchRegistrations = new ArrayList<>();
                if ("5".equals(targetType)) {//ios
                    if (j == patch) {
                        patchRegistrations = registrationIds.subList(j * patchNum, registrationIds.size());
                    } else {
                        patchRegistrations = registrationIds.subList(j * patchNum, (j + 1) * patchNum);
                    }
                    requestSendBean.setRegistrationIds(patchRegistrations);
                    jPushService.pushIos(requestSendBean, pushConfiguresPO, msgId, "5");

                } else if ("6".equals(targetType)) {//极光android
                    if (j == patch) {
                        patchRegistrations = registrationIds.subList(j * patchNum, registrationIds.size());
                    } else {
                        patchRegistrations = registrationIds.subList(j * patchNum, (j + 1) * patchNum);
                    }
                    requestSendBean.setRegistrationIds(patchRegistrations);
                    jPushService.pushAndroid(requestSendBean, pushConfiguresPO, msgId, "6", false);
                } else {//厂商通道
                    if (j == patch) {
                        patchTokens = tokens.subList(j * patchNum, tokens.size());
                        patchRegistrations = registrationIds.subList(j * patchNum, registrationIds.size());
                    } else {
                        patchTokens = tokens.subList(j * patchNum, (j + 1) * patchNum);
                        patchRegistrations = registrationIds.subList(j * patchNum, (j + 1) * patchNum);
                    }
                    requestSendBean.setRegistrationIds(patchRegistrations);
                    requestSendBean.setTokens(patchTokens);
                    pushService.pushAllMessage(requestSendBean, pushConfiguresPO, msgId, targetType);
                }
            }
        }
    }


    private void pushIosAllMsg(RequestSendBean requestSendBean, ArrayList<PushUserPO> userBeans, PushConfiguresPO pushConfiguresPO, String msgId) throws Exception {
        ArrayList<String> registIds = new ArrayList<>();
        for (int i = 0; i < userBeans.size(); i++) {
            if (!StringUtils.isEmpty(userBeans.get(i).getRegistrationId())) {
                registIds.add(userBeans.get(i).getRegistrationId());
            }
        }
        //批量发ios
        pushAllMsgByPatch(null, registIds, requestSendBean, pushConfiguresPO, null, msgId, "5");
    }

    private void pushAndroidBatchMsg(RequestSendBean requestSendBean, PushConfiguresPO pushConfiguresPO, String msgId) throws Exception {
        //添加声音
        if (!StringUtils.isEmpty(requestSendBean.getAndroidConfig())) {
            Map<String, String> extras = requestSendBean.getExtras();
            if (extras == null) {
                extras = new HashMap<>();
            }
            Object sound = requestSendBean.getAndroidConfig().get("sound");
            if (Objects.nonNull(sound)) {
                extras.put("sound", sound.toString());
            }
            requestSendBean.setExtras(extras);
        }
        if (StringUtils.isEmpty(requestSendBean.getPushType()) || "0".equals(requestSendBean.getPushType())) {
            if (requestSendBean.getDeviceType().equals(DeviceType.HW) && requestSendBean.getTokens().size() != 0) {
                huaweiPushService.pushBatchMessage(requestSendBean, pushConfiguresPO, msgId, "1");
            } else if (requestSendBean.getDeviceType().equals(DeviceType.XM) && requestSendBean.getTokens().size() != 0) {
                xiaomiPushService.pushBatchMessage(requestSendBean, pushConfiguresPO, msgId, "2");
            } else if (requestSendBean.getDeviceType().equals(DeviceType.OP) && requestSendBean.getTokens().size() != 0) {
                oppoPushService.pushBatchMessage(requestSendBean, pushConfiguresPO, msgId, "3");
            } else if (requestSendBean.getDeviceType().equals(DeviceType.VO) && requestSendBean.getTokens().size() != 0) {
                vivoPushService.pushBatchMessage(requestSendBean, pushConfiguresPO, msgId, "4");
            } else {//走极光
                if (requestSendBean.getRegistrationIds() == null || requestSendBean.getRegistrationIds().size() == 0) {
                    return;
                } else {
                    jPushService.pushAndroid(requestSendBean, pushConfiguresPO, msgId, "6", false);

                }
            }

        } else {//走极光
            if (requestSendBean.getRegistrationIds() == null || requestSendBean.getRegistrationIds().size() == 0) {
                return;
            } else {
                jPushService.pushAndroid(requestSendBean, pushConfiguresPO, msgId, "6", false);

            }
        }

    }


}
