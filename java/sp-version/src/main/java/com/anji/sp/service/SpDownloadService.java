package com.anji.sp.service;

import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by raodeming on 2021/8/20.
 */
public interface SpDownloadService {

    ResponseEntity<byte[]> download(HttpServletRequest request, HttpServletResponse response, String sdk);
}
