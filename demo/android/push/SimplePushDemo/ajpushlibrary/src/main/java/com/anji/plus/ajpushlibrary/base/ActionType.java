package com.anji.plus.ajpushlibrary.base;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * Appsp-Push 对消息处理的方式，点击或者不处理
 * </p>
 */
public class ActionType {
    public static final int NONE = 0x01;
    public static final int CLICK = 0x02;
}
