//
//  RequestManager.h
//  AJPushDemo
//
//  Created by kean_qi on 2021/3/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
//成功回调类型:参数: 1. id: object(如果是 JSON ,那么直接解析成OC中的数组或者字典.如果不是JSON ,直接返回 NSData)
typedef void(^SuccessBlock)(id object , NSURLResponse *response);
// 失败回调类型:参数: NSError error;
typedef void(^failBlock)(NSError *error);
@interface RequestManager : NSObject
// 单例的实例化方法
+ (instancetype)sharedInstance;
// GET请求调用的方法 其中: urlString:网络接口. paramaters:参数字典 参数字典:  key:服务器接收参数的 key 值.  value:参数内容. 成功回调类型:参数: 1. id: object(如果是 JSON ,那么直接解析成OC中的数组或者字典.如果不是JSON ,直接返回 NSData) 2. NSURLResponse: response ,  success: 成功回调. fail 失败回调.
- (void)GETRequestWithUrl:(NSString *)urlString paramaters:(NSMutableDictionary *)paramaters successBlock:(SuccessBlock)success FailBlock:(failBlock)fail;
// POST请求调用方法 其中: urlString:网络接口. paramaters:参数字典 参数字典:  key:服务器接收参数的 key 值.  value:参数内容. 成功回调类型:参数: 1. id: object(如果是 JSON ,那么直接解析成OC中的数组或者字典.如果不是JSON ,直接返回 NSData) 2. NSURLResponse: response ,  success: 成功回调. fail 失败回调.
-(void)POSTRequestWithUrl:(NSString *)urlString paramaters:(NSMutableDictionary *)paramaters successBlock:(SuccessBlock)success FailBlock:(failBlock)fail;
@end

NS_ASSUME_NONNULL_END
